'use strict';

console.log('App.js is running');

// JSX - Javascript XML
var template = React.createElement(
  'h1',
  null,
  'This is JSX from app.js ooo'
);
// sem babel so da se ficar com o seguinte formato:
/*
var template = React.createElement(
  'h1',
  {id: 'someid'},
  'Something new'
);
*/

var appRoot = document.getElementById('app');

// render the template in the appRoot element
ReactDOM.render(template, appRoot);

// instalar babel globalmente
// yarn global add babel-cli

// yarn init ou npm init para iniciar para usar dependencias no projeto

// instalar dependencias localmennte
// yarn add babel-preset-react babel-preset-env

// babel <path_to_our_code_want_to_compile> --out-file=<output_path_file> --presets=env,react --watch
